function export_my_shortlist( shortlist )
%EXPORT_MY_SHORTLIST Exports the shortlisted articles in to a .txt file

clc;

diary('shortlisted_articles')

contents = fields(shortlist);
elements = fields(shortlist.(contents{1}));

for i_content = 1:length(contents)
    
    fprintf('%s\n\n',contents{i_content});
    
    for i_element = 1:length(elements)
        
        fprintf('%s: ',elements{i_element});
        
        for i_lines = 1:size(shortlist.(contents{i_content}).(elements{i_element}),1)
            
            wraptext([char(shortlist.(contents{i_content}).(elements{i_element})(i_lines,:)), ' '],80);
            
        end
        
        fprintf('\n');
    end
    
    fprintf('\n');
    
end

diary off

end