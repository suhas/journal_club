function all_links = get_all_article_links(link_to_issue, this_journal)

fprintf('Fetching all article links \n');

html = webread(char(link_to_issue));
% Use regular expressions to remove undesired markup.
txt = regexprep(html,'<script.*?/script>','');
txt = regexprep(txt,'<style.*?/style>','');

switch upper(this_journal)

    case 'PNAS'
        
        all_links_start     = '<span  class="highwire-cite-metadata-doi highwire-cite-metadata">';
        all_links_end       = ' </span></div>';
        all_links           = extractBetween(txt,all_links_start,all_links_end);
        
    case 'JOURNAL_OF_NEUROPHYSIOLOGY'
        
        all_links_start     = '<a title="Abstract" href="';
        all_links_end       = '"><span>';
        all_partial_links   = extractBetween(txt,all_links_start,all_links_end);
        base_url            = 'https://www.physiology.org';
        
        for i_link = 1:length(all_partial_links)
            all_links{i_link} = cellstr([base_url char(all_partial_links{i_link})]);
        end
        
    case 'NEUROIMAGE'
        
        all_links_start     = 'u-margin-right-m text-s" href="/science/article/pii/';
        all_links_end       = '/pdfft?md5=';
        all_partial_links   = extractBetween(txt,all_links_start,all_links_end);
        base_url            = 'https://www.sciencedirect.com/science/article/pii/';
        
        for i_link = 1:length(all_partial_links)
            all_links{i_link} = cellstr([base_url char(all_partial_links{i_link})]);
        end        
        
    case 'JOURNAL_OF_NEUROSCIENCE'
        
        all_links_start     = '<span  class="highwire-cite-metadata-doi highwire-cite-metadata">DOI: ';
        all_links_end       = ' </span></div>';
        all_links           = extractBetween(txt,all_links_start,all_links_end);
        
end

end