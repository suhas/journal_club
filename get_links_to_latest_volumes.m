function links = get_links_to_latest_volumes(this_journal)

links = {};

switch upper(this_journal)
    
    case 'PNAS'
        
        fprintf('Fetching links from %s \n',char(this_journal));
        
        url_to_all_volumes = 'http://www.pnas.org/content/by/volume';
        base_url = 'http://www.pnas.org';
        html = webread(url_to_all_volumes);
        % Use regular expressions to remove undesired markup.
        txt = regexprep(html,'<script.*?/script>','');
        txt = regexprep(txt,'<style.*?/style>','');
        
        issue_start = 'div class="highwire-cite-metadata"><a class="hw-issue-meta-data" href="';
        issue_end = '"><span  class="highwire-cite-metadata-pub-date highwire-cite-metadata">';
        title = extractBetween(txt,issue_start,issue_end);
        
        for i_link = 1:4
            links{i_link} = cellstr([base_url char(title{i_link})]);
        end
        
    case 'NEUROIMAGE'
        
        fprintf('Fetching links from %s \n',char(this_journal));
        
        
        url_to_all_volumes = 'https://www.sciencedirect.com/journal/neuroimage/issues';
        base_url = 'https://www.sciencedirect.com/journal/neuroimage';
        html = webread(url_to_all_volumes);
        % Use regular expressions to remove undesired markup.
        txt = regexprep(html,'<script.*?/script>','');
        txt = regexprep(txt,'<style.*?/style>','');
        
        issue_start = '<a class="anchor text-m" href="/journal/neuroimage';
        issue_end = '"><span class="anchor-text">Volume';
        title = extractBetween(txt,issue_start,issue_end);
        
        for i_link = 1:1
            links{i_link} = cellstr([base_url char(title{i_link})]);
        end
        
        
    case 'JOURNAL_OF_NEUROSCIENCE'
        
        fprintf('Fetching links from %s \n',char(this_journal));
        
        url_to_all_volumes = 'http://www.jneurosci.org/content/by/year';
        base_url = 'http://www.jneurosci.org';
        html = webread(url_to_all_volumes);
        % Use regular expressions to remove undesired markup.
        txt = regexprep(html,'<script.*?/script>','');
        txt = regexprep(txt,'<style.*?/style>','');
        
        issue_start = 'div class="highwire-cite-metadata"><a class="hw-issue-meta-data" href="';
        issue_end = '"><span  class="highwire-cite-metadata-pub-date highwire-cite-metadata">';
        title = extractBetween(txt,issue_start,issue_end);
        
        for i_link = 1:4
            links{i_link} = cellstr([base_url char(title{i_link})]);
        end
        
    case 'JOURNAL_OF_NEUROPHYSIOLOGY'
        
        fprintf('Fetching links from %s \n',char(this_journal));
        
        url_to_current_vol = 'https://www.physiology.org/toc/jn/current';
        
        links = cellstr(url_to_current_vol);
        
    otherwise
        
        error('Unknown journal!');
        
end % switch

end % function