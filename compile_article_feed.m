function compile_article_feed

clear; clc

journals.name = {'PNAS','Neuroimage','Journal_of_Neuroscience','Journal_of_Neurophysiology'};
keywords        = {'neuroscience','human','brain','social','motor','frontal','temporal','parietal','fMRI'};

%==========================================================================
% Get links to latest issues
%==========================================================================
% function call: get_links_to_latest_valumes
for i_journal = 1:length(journals.name)
    journals.(journals.name{i_journal}).latest_volume_links{i_journal} = get_links_to_latest_volumes(journals.name{i_journal});
end

%==========================================================================
% Get links to all articles in the issue
%==========================================================================
% function call: get_all_article_links
for i_journal = 1:length(journals.name)
    for i_volume = 1:length(journals.(journals.name{i_journal}).latest_volume_links)
        for i_issue = 1:length(journals.(journals.name{i_journal}).latest_volume_links{i_volume})
            journals.(journals.name{i_journal}).article_links{i_issue} = ...
                get_all_article_links(journals.(journals.name{i_journal}).latest_volume_links{i_volume}{i_issue}, journals.name{i_journal});
        end
    end
end

%==========================================================================
% Get title, abstract, keywords
%==========================================================================
% function call: get_article_details
for i_journal = 1:length(journals.name)
    fprintf('Fetching links from %s | ',char(journals.name{i_journal}));
    for i_volume = 1:length(journals.(journals.name{i_journal}).latest_volume_links)
        fprintf('Volume %d | ',i_volume);
        for i_issue = 1:length(journals.(journals.name{i_journal}).latest_volume_links{i_volume})
            fprintf('Issue %d \n\n',i_issue);
            for i_article = 1:length(journals.(journals.name{i_journal}).article_links{i_issue})
                fprintf('Fetching article %d \n',i_article);
                journals.(journals.name{i_journal}).(['vol_', num2str(i_volume)]).(['issue_', num2str(i_issue)]).(['article_' num2str(i_article)]) = ...
                    get_article_details(journals.(journals.name{i_journal}).article_links{i_issue}{i_article});
            end
        end
    end
end

%==========================================================================
% Look for relevant articles
%==========================================================================
% function call: none
for i_journal = 1:length(journals.name)
    fprintf('Looking in %s | ',char(journals.name{i_journal}));
    for i_volume = 1:length(journals.(journals.name{i_journal}).latest_volume_links)
        fprintf('Volume %d | ',i_volume);
        for i_issue = 1:length(journals.(journals.name{i_journal}).latest_volume_links{i_volume})
            
            fprintf('Issue %d \n\n',i_issue);
            journals.(journals.name{i_journal}).(['vol_', num2str(i_volume)]).(['issue_', num2str(i_issue), '_logical'])(length(journals.(journals.name{i_journal}).article_links{i_issue}),1) = false;
            
            for i_article = 1:length(journals.(journals.name{i_journal}).article_links{i_issue})
                fprintf('Searching article %d \n',i_article);
                
                if ~journals.(journals.name{i_journal}).(['vol_', num2str(i_volume)]).(['issue_', num2str(i_issue), '_logical'])(i_article,1)
                    
                    abstract = journals.(journals.name{i_journal}).(['vol_', num2str(i_volume)]).(['issue_', num2str(i_issue)]).(['article_' num2str(i_article)]).abstract;
                    
                    for i_keyword = 1:length(keywords)
                        if sum(contains(abstract,keywords{i_keyword},'IgnoreCase',true)) > 0
                            journals.(journals.name{i_journal}).(['vol_', num2str(i_volume)]).(['issue_', num2str(i_issue), '_logical'])(i_article,1) = true;
                        end
                    end
                    
                end
            end
        end
    end
end

save results_journals journals

%==========================================================================
% Compile final list depending on search results
%==========================================================================
% function call: none
shortlist = [];

for i_journal = 1:length(journals.name)
    
    for i_volume = 1:length(journals.(journals.name{i_journal}).latest_volume_links)
        
        for i_issue = 1:length(journals.(journals.name{i_journal}).latest_volume_links{i_volume})
            
            fetch_these = find(journals.(journals.name{i_journal}).(['vol_', num2str(i_volume)]).(['issue_', num2str(i_issue), '_logical']));
            
            for i_article = 1:length(fetch_these)                                
                
                shortlist.(['article_' num2str(i_article)]) = journals.(journals.name{i_journal}).(['vol_', num2str(i_volume)]).(['issue_', num2str(i_issue)]).(['article_' num2str(fetch_these(i_article))]);
                
            end
            
        end
    end
end

export_my_shortlist(shortlist);

save shortlisted_articles shortlist

end