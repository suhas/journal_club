function article = get_article_details(article_link)

options = weboptions('Timeout', 42);
article = [];

url = char(article_link);
url = regexprep( url, '<.*?>', '' );
html = webread(url, options);
% Use regular expressions to remove undesired markup.
txt = regexprep(html,'<script.*?/script>','');
txt = regexprep(txt,'<style.*?/style>','');

title_start = 'meta name="citation_title" content="';
title_end = '" />';
title = extractBetween(txt,title_start,title_end);
if isempty(title)
    title_start = 'name="dc.Title" content="';
    title_end = '" />';
    title = extractBetween(txt,title_start,title_end);
end

abstract_start = 'name="DC.Description" content="';
abstract_end = '" />';
abstract = extractBetween(txt,abstract_start,abstract_end);
if isempty(abstract)
    abstract_start = 'name="dc.Description" content="';
    abstract_end = '" />';
    abstract = extractBetween(txt,abstract_start,abstract_end);
end
if isempty(abstract)
    abstract_start = 'Abstract</h2><div id="abssec0010"><p id="abspara0010">';
    abstract_end = '</p></div>';
    abstract = extractBetween(txt,abstract_start,abstract_end);
end

%==========================================================================
% Could get keywords, but looks like it's a bit difficult to implement.
%==========================================================================

link = url;

% Collect data
article.title       = title;
article.abstract    = abstract;
article.link        = link;
% article.keywords    = keywords;

end